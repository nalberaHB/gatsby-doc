# ¿Qué es GatsbyJs? y ¿para qué sirve?

Gatsby, o GatsbyJS, es un generador de sitios estáticos construido con ReactJS y alimentado por `GraphQL`. Gatsby permite a desarrolladores crear sitios web con React rápidamente.

Este framework incluye herramientas para crear consultas de datos y transformar esas consultas en componentes reutilizables.

## ¿Cómo funciona Gatsby?

Gatsby usa `GraphQL` para recolectar la información de nuestro sitio web desde diferentes fuentes: APIs, CMS o nuestro sistema de archivos. Y teniendo lista la información, renderiza nuestras vistas en React.js para construir sitios estáticos muy optimizados.

<img src="./images/image_1.png">

## Configuración del entorno

Para poder empezar a crear sitios con `Gatsby`, al tratarse de un fremework React, necesitaremos:

        - NodeJs
        - Git
        - VS Code (o el editor que prefiera) y
        - La interfaz de comandos en linea (CLI) de Gatsby

### CLI de Gatsby

La interfaz de línea de comandos (CLI) de Gatsby es una herramienta que nos permite crear rápidamente nuevos sitios basados ​​en Gatsby y ejecutar comandos.
La CLI es un paquete npm, lo que significa que puede instalarlo usando npm.

👉**Paso 1**
Abrir la consola de comandos y ejecutar:

```
npm install -g gatsby-cli
```

👉**Paso 2**
Comprobar que se tiene instalada la versión correcta ejecutando el siguiente comando.
Desde la consola de comandos, ejecutar:

```
gatsby --version
```

Deberías tener la versión 3 o más reciente.

<img src="./images/captura_patalla1.png">

## Crear un sitio Gatsby
Para crear un sitio con Gatsby, utilizaremos un comando de la interfaz de línea de comandos (CLI) de Gatsby: `gatsby new`. Este comando muestra un mensaje interactivo que le hace preguntas sobre el sitio que desea crear.

### Pasos para crear un proyecto (sitio) con Gatsby

👉**Paso 1**
Abra la consola y use el comando `cd` para cambiar los directorios a la carpeta donde se desea crear el nuevo proyecto. Por ejemplo, para crear su nuevo sitio en el escritorio, puede escribir:

```
cd Desktop

O...

cd Escritorio (si el sistema está en Español)
```

👉**Paso 2**
Ejecutar el comando `gatsby new` desde la línea de comandos. Esto iniciará el mensaje interactivo para ayudarle a crear un nuevo sitio de Gatsby.

```
gatsby new
```

```
Nota: Si hubo problemas para realizar la instalación global gatsby-cli, y se pudieron solucionar, también puede crear un uevo proyecto ejecutando `npm init gatsby` en lugar de `gatsby new`
```

👉**Paso 2.1**
What would you like to call your site? Ingresamos el nombre de nuestro sitio:
Ej: `Primer sitio con Gatsby` o `Primer proyecto con Gatsby` o `Aprendiendo Gatsby`

Notese que esto es un nombre para el `sitio`, de ahí los espacios en blanco.


👉**Paso 2.2**
Luego nos preguntará como queremos que se llama la `carpeta` en donde se va crear nuestro sitio

`What would you like to name the folder where your site will be created?`

Acá usaremos el nombre de carpeta predeterminado, que se basará en el nombre del sitio elegido.
(Puede pulsar la tecla 'TAB' esto seleccionará automáticamente el nombre predeterminado)


👉**Paso 2.3**
A la pregunta si queremos usar `JavaScript` o `TypeScript`, por el momento, vamos a seleccionar `JavaScript`
```
Will you be using JavaScript or TypeScript?
❯ JavaScript
  TypeScript
```

👉**Paso 2.4**
Luego nos preguntará si vamos a utilizar algún `CMS`, por el momento le diremos que no
o lo agragaremos después:
```
No (or I'll add it later)
```

👉**Paso 2.5**
La siguiente pregunta hace referencia a al sistema de estilos `styling system`, si vamos a instalar algún sistema específico, o si lo instalaremos después. En este caso, como en el anterior, le diremos que lo agragaremos después:
```
✔ Would you like to install a styling system?
· No (or I'll add it later)

```

👉**Paso 2.6**
Lo próximo será decirle al instalador si vamos a querer instalar funcionalidades especiales con algunos complementos (plugins). Por el momento, simplemente nos moveremos con las flechas a la opción `Done`

```
✔ Would you like to install additional features with other plugins?
· Done
```

👉**Paso 2.7**
Se nos mostrará un resumen de lo que el comando `gatsby new` hará.
Debería aparecer lo siguiente:

```
Thanks! Here's what we'll now do:

    🛠  Create a new Gatsby site in the folder my-first-gatsby-site


? Shall we do this? (Y/n) › Yes
```

Respondemos con `Yes`

Finalmente, esto dará comienzo al proceso de instalación.

La instalación finaliza mostrando el siguiente mensaje:

```
🎉  Your new Gatsby site Mi primer sitio con Gatsby has been successfully created
at /home/nalbera/Escritorio/mi-primer-sitio-con-gatsby.
Start by going to the directory with

  cd mi-primer-sitio-con-gatsby

Start the local development server with

  npm run develop

See all commands at

  https://www.gatsbyjs.com/docs/reference/gatsby-cli/

```

👉**Paso 3**

Entramos al directorio (carpeta) que el instalador nos acaba de crear y abrimos el proyecto en `VS Code`

```
cd mi-primer-sitio-con-gatsby/

code .
```

👉**Paso 4**
Dentro de `VS Code` abrimos la terminal y ejecutamos el servidor de desarrollo en forma local.

```
gatsby develop
```

Si no se pudo instalar la interfaz de línea de comandos de Gatsby golbalmente, se puede iniciar el servidor de desarrollo con `npm run develop`.

Después de unos momentos nos expodrá un mensaje como el siguiente:

```
You can now view my-first-gatsby-site in the browser.
⠀
  http://localhost:8000/
⠀
View GraphiQL, an in-browser IDE, to explore your site's data and
schema
⠀
  http://localhost:8000/___graphql
```
👉**Paso 5**
Abrimos el navegador y "navegamos" hasta `http://localhost:8000`

## Estructura de archivos de Gatsby

Cuando abras tu proyecto en un editor de código, verás la siguiente estructura:

<img src="./images/estructura_de_archivos.png">

- `./public`: Esta carpeta contiene el resultado del proceso de compilación de Gatsby. Es donde residen el HTML, CSS, JavaScript y otros recursos generados.

- `./src`: Es el corazón de un proyecto Gatsby.

- `./src/page`: Almacena todas las páginas del proyecto. Cada archivo JavaScript aquí corresponde a una ruta de tu sitio web.

- `gatsby-config.js`: Este archivo exporta un objeto de configuración en donde se definen varios ajustes para Gatsby. Ejemplo: especificar plugins, metadatos del sitio y otras configuraciones.


# Gatsby Head API
Se puede utilizar la <a href="https://www.gatsbyjs.com/docs/reference/built-in-components/gatsby-head/">Gatsby Head API</a> exportando una función `Head` dentro de la página.
(La función debe llamarse `Head` con mayúscula)

Ejemplo:

```javascript
import * as React from "react"

const Page = () => <div>Hola Mundo</div>
export default Page

export function Head() {
  return (
    <title>Hola Mundo</title>
    <meta name="description" content="Contenido de la descripción" />
  )
}
```

O podría ser como una `arrow function`:

```javascript
import * as React from "react"

const Page = () => <div>Hola Mundo</div>
export default Page

export const Head = () => (
  <>
    <title>About Me</title>
    <meta name="description" content="Your description" />
  </>
)
```
# Crear componente reutilizable (layout)

La función de un componente `layout` es agrupar todos aquellos elementos que son comunes a todas las páginas del sitio y compartilos entre ellas.

Vamos a crear un componente `layout` y agregarlo a nuestras páginas de inicio y about.

👉**Paso 1**
Crear un nuevo archivo llamado `src/components/layout.jsx`, y copiemos el siguiente código

**`src/components/layout.jsx`**
```javascript
import * as React from 'react'
import { Link } from 'gatsby'

const Layout = ({ title, children }) => {
  return (
    <div>
      <nav>
        <ul>
          <li><Link to="/">Home</Link></li>
          <li><Link to="/about">About</Link></li>
        </ul>
      </nav>
      <main>
        <h1>{title}</h1>
        {children}
      </main>
    </div>
  )
}

export default Layout;
```

👉**Paso 2**
Actualicemos el componente `index`

👉**Paso 2.1**
**`src/pages/index.jsx`**
```javascript
//...
import Layout from '../components/layout'
//...
```
👉**Paso 2.2**
"Envolvemos" nuestro componente con el `layout`
**`src/pages/index.jsx`**
```javascript
//...

  <Layout title='Home Page'>
        <main>
          <h1>Bienvenidos al sitio de Gatsby</h1>
          <p>Este es un componente envuelto en un layout</p>
        </main>
  </Layout>

```

```
Repetimos los pasos anteriores, ahora para el `About`.
```

# Agregar funciones con complementos

`Gatsby`, posee una serie de `plugins` ***(complementos)*** que puede ser utilizados para agregar nuevas funciones a un sitio sin crearlas desde cero.
El ecosistema de complementos de Gatsby tiene miles de paquetes prediseñados entre los que puede elegir para cumplir con estas "tareas".

## ¿Qué es un complemento? (plugin)

Un complemento es un paquete npm independiente que instala para agregar funciones adicionales a un sitio.
Hay una variedad de complementos y cada uno tiene diferentes casos de uso. Algunos complementos proporcionan componentes prediseñados, otros agregan análisis y otros le permiten extraer datos de un sitio.

### Agregar un complemento

Para este ejemplo usaremos el `gatsby-plugin-image` para agregar una imágen estática a nuestro sitio.

Para agregar un complemento al proyecto, seguiremos los siguientes pasos:

👉**Paso 1 Instalar el complemento usando `npm`**

En la terminal escriba el siguiente comando `gatsby-plugin-image` y sus dependencias.

```
npm install gatsby-plugin-image gatsby-plugin-sharp gatsby-source-filesystem
```

- `gatsby-plugin-sharp`: Maneja el procesamiento de imágenes real que utiliza `gatsby-plugin-image`.
- `gatsby-source-filesystem`: Le permite extraer datos del sistema de archivos de su computadora.


👉**Paso 2 Configurar el complemento en el archivo `gatsby-config.js`**

**`gatsby-config.js`**
```javascript
module.exports = {
  siteMetadata: {
    title: `Mi primer sitio con Gatsby`,
    siteUrl: `https://www.yourdomain.tld`,
  },
  plugins: [
    "gatsby-plugin-image",
    "gatsby-plugin-sharp",
  ],
}
```

👉**Paso 3 Utilizar las funciones del complemento en su sitio, donde sea necesario.**
En esta caso lo utilizaremos en nuestro `index.jsx`.

👉**Paso 3.1 Importamos {StaticImage}**
**`src/pages/index.js`**
```javascript
//...
import { StaticImage } from 'gatsby-plugin-image'
```

👉**Paso 3.2 Insertamos el componente StaticImage**
```javascript
//...

<Layout title='Home Page'>
        <main>
          <h1>Bienvenidos al sitio de Gatsby</h1>
          <p>Este es un componente envuelto en un layout</p>
            <StaticImage
              alt="Imagen del dog"
              src="../images/dog_1.jpeg"
          />
        </main>
</Layout>

//...
```
