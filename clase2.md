# Consulta de datos con GraphQL
Gatsby tiene una poderosa característica llamada `capa de datos`, que se puede usar para extraer datos desde cualquier lugar.
Esta capa de datos funciona con una tecnología llamada `GraphQL`, que se trata de un lenguaje de consulta con una sintaxis especial que permite solicitar datos que necesitan los componentes desde distintas fuentes, carpetas de archivos de la computadora, un CMS como WordPress o una base de datos o de todas ellas juntas.

## ¿Como se obtienen los datos?
Agragando un tipo de plugin llamado `plugin de origen`. Cada plugin funte está diseñado para comunicarse con una fuente específica. Cuando crea un sitio, cada complemento de origen extrae datos de su fuente particular y los agrega a la capa de datos GraphQL del sitio.

<img src="./images/data-layer.png">

**Para obtener más información sobre plugins ingresar a la <a href="https://www.gatsbyjs.com/plugins">biblioteca de plugins</a>. Los complementos de origen empiezan todos con gatsby-source-**

## Consultas a componentes básicos
Veremos el proceso de como generar una consulta básica a un componente con `GraphQL`.
Integraremos, como ejemplo, dos componentes básicos.
Extraeremos información del `layout` y crearemos un componnete para mostrar el título del sitio.

👉**Paso 1**
Veamos el archivo `gatsby-config.js`

**`/gatsby-config.js`**
```javascript
module.exports = {
  siteMetadata: {
    title: `Mi primer sitio con Gatsby`,
    siteUrl: `https://www.yourdomain.tld`,
  },
  plugins: [
    "gatsby-plugin-image",
    "gatsby-plugin-sharp",
  ],
}
```
Estos datos fueron agregado automáticamente cuando se creó el proyecto con `gatsby new`.
Como para esta consula, no necesitamos confirurar ningún complemento `(plugin)`, podemos entrar directamente a el editor de `GraphQL` que trae incorporado el mismo `Gatby` entrando en la dirección `http://localhost:8000/___graphql` (dos barras bajas).

<img src="./images/graph_1.png">

👉**Paso 2**
En el explorador abrimos el menú `site` y navegamos hasta `siteMetadata` (el azul) y seleccionamos la opción `title`

<img src="./images/graph_2.png">

👉**Paso 3**
Hacemos `click` en el botón ejecutar ("triángulo reproducir). Esto ejecutará la consula y el resultado se verá en la ventana de `resultados` a la derecha de la pantalla.

<img src="./images/graph_3.png">

Ahora que tenemos la consulta `GraphQL` podemos extraer estos datos básicos de un componente con el uso de la función `useStaticQuery`

👉**Paso 4**
Importamos la función `useStaticQuery` en el componente `layout.jsx` además de `graphql` de Gatsby

**`./src/components/layout.jsx`**

```javascript
//...
import { Link, useStaticQuery, graphql } from 'gatsby'

//...
```

👉**Paso 5**
Llamamos a la función `useStaticQuery` y le  pasamos la consulta que creamos en `GraphQL`.
Asegurándose de usar la etiqueta `graphql` para que `Gatsby` sepa que la cadena que se está pasando es una consulta GraphQL.

**`./src/components/layout.jsx`**

```javascript
//...

const Layout = ({ title, children }) => {

  const data = useStaticQuery(graphql`
      query {
        site {
          siteMetadata {
            title
          }
        }
      }
  `)

  //....
}
//...
```

👉**Paso 6**
Ahora que tenemos el resultado de la consulta en la constante `data`, podemos representarla en el componente.

**`./src/components/layout.jsx`**

```javascript
//...

 return (
    <div className={container}>
      <header>{data.site.siteMetadata.title}</header>
      <nav>
        <ul className={navLinks}>
          <li className={navLinkItem}>
            <Link to="/" className={navLinkText}>Home</Link>
          </li>
          <li className={navLinkItem}>
            <Link to="/about" className={navLinkText}>About</Link>
          </li>
          <li className={navLinkItem}>
            <Link to="/contacts" className={navLinkText}>Contacts</Link>
          </li>
        </ul>
      </nav>
      <main>
        <h1 className={heading}>{ title }</h1>
        {children}
      </main>
    </div>
  )

//...
```

## Consultas en componentes de página
Hasta ahora solamente hemos creado algunas páginas de destino estáticas.
Veremos como hacer vinculaciones a páginas en forma dinámica.
Para ello crearemos la etructura de una página de `blog` y agragaremos un nuevo enlace.

👉**Paso 1**
Crear la estructura de una página que servirá para mostrar las entradas de un `blog`.

**`src/pages/blog.jsx`**

```javascript
import * as React from 'react'
import Layout from '../components/layout'

const BlogPage = () => {
  return (
    <Layout pageTitle="Entradas del Blog">
      <p>Listado de entradas del blog</p>
    </Layout>
  )
}

export const Head = () => <title>"Entradas del Blog"</title>

export default BlogPage
```

👉**Paso 2**
Crear la estructura de una página que servirá para mostrar las entradas de un `blog`.

**`src/components/layout.js`**

```javascript
//...
<li className={navLinkItem}>
    <Link to="/blog" className={navLinkText}>
        Blog
    </Link>
</li>
//...
```

👉**Paso 3.1**
Vamos a crear tres archivos dentro de una carpeta llamada `blog` con extensión `mdx` que almacenará cada publicación del blog como archivos separados. Esta carpeta la crearemos en el nivel superior de la carpeta `src`.

<img src="./images/estructura_2.png">

👉**Paso 3.2**
Dentro de la carpeta blog creamos 3 archivos `entrada1.mdx`, `entrada2.mdx` y `entrada3.mdx`

**`/blog`**

<img src="./images/estructura_3.png">

👉**Paso 4**
Utilizando el `plugin` `gatsby-source-filesystem` crearemos una consulta `GraphQL` para colocar esos archivos en la capa de datos de Gatsby.

👉**Paso 4.1**
En la consola:

```
npm install gatsby-source-filesystem
```

👉**Paso 4.2**
Configurar el nuevo `plugin` en `gatsby-config.js`.
Dado que `gatsby-source-filesystem` requiere algunas opciones de configuración adicionales, se utilizará un objeto de configuración en lugar de una cadena. 

**`gatsby-config.js`**

```javascript
//...
plugins: [
    "gatsby-plugin-image",
    "gatsby-plugin-sharp",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: `blog`,
        path: `${__dirname}/blog`,
      }
    },
  ],
```

- Cuando se carga el sitio `gatsby-source-filesystem` agrega todos los archivos especificados en la carpeta especificada por `path`.

- La opción `name` establece un campo `sourceInstanceName` para cada archivo. Esto es muy útil para cuando se obtienen achivos de varias carpetas.

👉**Paso 4.3**
Exploremos la opción `allFile` de GraphQL para solicitar varios archivos a la vez.

```
query MyQuery {
  allFile {
    nodes {
      name
    }
  }
}
```
Si ejecutamos la consulata nos devolverá la siquiente salida:

```json
{
  "data": {
    "allFile": {
      "nodes": [
        {
          "name": "entrada1"
        },
        {
          "name": "entrada2"
        },
        {
          "name": "entrada3"
        }
      ]
    }
  },
  "extensions": {}
}
```

👉**Paso 4.4**
Agreguemos la consulta a la página `blog`

**`src/pages/blog.js`**

```javascript
//...
import { graphql } from 'gatsby'
//...


export const query = graphql`
  query {
    allFile {
      nodes {
        name
      }
    }
  }
`
export const Head = () => <title>"Entradas del Blog"</title>

export default BlogPage
```

👉**Paso 4.4**
Y por último modifiquemos la función `BlogPage` de la siguiente manera:

```javascript
//...
const BlogPage = ({ data }) => {
  return (
    <Layout pageTitle="Entradas del Blog">
        <ul>
            {
                data.allFile.nodes.map((node) => (
                    <li key={node.name}>
                        {node.name}
                    </li>
                ))
            }
        </ul>
      
    </Layout>
  )
}
//...
```
Guardemos el archivo y actualicemos el navegador deberíamos ver lo siguiente:

<img src="./images/salida_archivos.png">

## Transformar datos para usar MDX
En el punto anterior, se utilizó` gatsby-source-filesystem` para crear una página de blog que enumera los nombres de los archivos de publicaciones.
Pero en realidad no representó el contenido porque `gatsby-source-filesystem` no proporciona un campo para ello.
Para hacer eso, nececitamos de otro tipo de `plugin` llamado transformador.

Primero agreguemos contenido a los archivos `mdx` en formato markdown y frontmatter.

Ejemplo debería verse así:

**`blog/entrada1.mdx`**
```md
---
title: "Mi primer Post"
date: "2024-03-20"
slug: "my-first-post"
---

Este es mi primer post

Algunas de mis actvidades **favoritas** son:

* Caminar
* Ir de compras
* Y las papas fritas!
```
Completar los otros dos archivos restantes, con ese mismo formato, cambiando algunas partes para que se muestre otro tipo de información.

###  Instalar y configurar el `plugin` transformador `MDX` y sus dependencias

👉**Paso 1**
En la consola del sistema, correr el siguiente comando:

```
npm install gatsby-plugin-mdx @mdx-js/react
```

👉**Paso 2**
Agregamos el `plugin` `gatsby-plugin-mdxa` en el archivo `gatsby-config.js`, para que Gatsby sepa cómo usar este complemento al crear el sitio.

**`gatsby-config.js`**

```javascript
plugins: [
    "gatsby-plugin-image",
    "gatsby-plugin-sharp",
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `blog`,
        path: `${__dirname}/blog/`,
      },
    },
    "gatsby-plugin-mdx",
  ],
```
👉**Paso 3**
Escribamos la consulta nuevamente en `GraphQL` con el campo `allMdx` en lugar de `allFile`

**`GraphQL`**

```
query MyQuery {
  allMdx {
    nodes{
      frontmatter {
        date(formatString: "MMMM D, YYYY")
        title
      }
      id
      excerpt
    }
  }
}

```
Ejecutemos la consulta nos debería mostrar lo siguiente:

```json
{
  "data": {
    "allMdx": {
      "nodes": [
        {
          "frontmatter": {
            "date": "March 20, 2024",
            "title": "Mi primer Post"
          },
          "id": "7bbe99ab-5015-598d-b462-8e17c5e9e9a3",
          "excerpt": "Este es mi primer post Algunas de mis actvidades favoritas son:\n\nCaminar Ir de compras Y las papas fritas!"
        },
        {
          "frontmatter": {
            "date": "March 10, 2024",
            "title": "Este es otro post"
          },
          "id": "9f34f49d-61ee-5079-8b82-0a5dfe7913aa",
          "excerpt": "Pero no tiene nada de contenido, solo esta frase"
        },
        {
          "frontmatter": {
            "date": "March 10, 2024",
            "title": "Este es mi tercer post"
          },
          "id": "a7dc398e-46ef-53b7-88bd-1f1c05da5755",
          "excerpt": "Pero no tiene nada de contenido, solo esta frase"
        }
      ]
    }
  },
  "extensions": {}
}
```
👉**Paso 4**
Modificamos la consulta y el map en el archivo de blog

**`src/pages/blog.js`**

```javascript
//...
const BlogPage = ({ data }) => {
  return (
    <Layout pageTitle="Entradas del Blog">
        <ul>
            {
              data.allMdx.nodes.map((node) => (
                <article key={node.id}>
                    <h2>{node.frontmatter.title}</h2>
                    <p>Posted: {node.frontmatter.date}</p>
                </article>
             ))
            }
        </ul>
      
    </Layout>
  )
}

export const query = graphql`
  query {
    allMdx(sort: { frontmatter: { date: DESC }}) {
      nodes {
        frontmatter {
          date(formatString: "MMMM D, YYYY")
          title
        }
        id
        excerpt
      }
    }
  }
 `
 //...
```


